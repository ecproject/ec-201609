<?php

class ProductSeeder extends Seeder {

    public function run()
    {

        //商品
        $this->db->truncate('product');

        $data = [
            'name' => 'PHP+MySQLマスターブック',
            'price' => 2916,
            'author' => '永田 順伸',
            'stock' => 2,
            'body' => 'Very good CodeIgniter book.',
            'description' => '本書は、この一冊でPHPとMySQLの基本とWebアプリケーションの構築法について学習できる実践的なプログラミング入門です。'
        ];
        $this->db->insert('product', $data);

        $data = [
            'name' => 'いきなりはじめるPHP',
            'price' => 1944,
            'author' => '谷藤賢一',
            'stock' => 4,
            'body' => 'PHPのいちばんやさしい入門書でありながら、MySQLまでカバーしています。',
            'description' => '初心者がつまずいたり迷ったりするポイントをとことん解消しました。面倒なパソコン設定もデータベース構築も楽々クリアできます。実際の研修講座で実証済みの方法論とカリュキュラムにより、無理なく楽しくPHPのエッセンスを習得することができます。'
        ];
        $this->db->insert('product', $data);

        $data = [
            'name' => 'いまどきのアルゴリズムを使いこなす PHPプログラミング開発テクニック',
            'price' => 3218,
            'author' => 'クジラ飛行机',
            'stock' => 4,
            'body' => 'アルゴリズムの重要性は、今も昔も変わりません。',
            'description' => '今日、PHPはWebの中核をなすプログラミング言語となりました。世界中でPHPが使われているということは、さまざまなロジックやアルゴリズムをPHPで書く機会が増えていることでもあります。そこで、本書では、Webアプリ開発で役立つであろう「イマドキのアルゴリズム」を紹介していきます。'
        ];
        $this->db->insert('product', $data);







    }

}
