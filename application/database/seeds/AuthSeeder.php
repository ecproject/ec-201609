<?php

class AuthSeeder extends Seeder {

    public function run()
    {

        //【ログイン関連】グループ
        $this->db->truncate('groups');

        $groups_data = [
            'id' => '1',
            'name' => 'admin',
            'description' => 'Administrator'
        ];
        $this->db->insert('groups', $groups_data);
        $groups_data = [
            'id' => '2',
            'name' => 'members',
            'description' => 'General User'
        ];
        $this->db->insert('groups', $groups_data);

        //【ログイン関連】ユーザー
        $this->db->truncate('users');

        $groups_data = [
            'id' => '1',
            'ip_address' => '127.0.0.1',
            'username' => 'administrator',
            'password' => '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36',
            'salt' => '',
            'email' => 'admin@admin.com',
            'activation_code' => '',
            'forgotten_password_code' => NULL,
            'created_on' => '1268889823',
            'last_login' => '1268889823',
            'active' => '1',
            'first_name' => 'Admin',
            'last_name' => 'istrator',
            'company' => 'ADMIN',
            'phone' => '0',
        ];
        $this->db->insert('users', $groups_data);

        //【ログイン関連】ユーザーグループ
        $this->db->truncate('users_groups');

        $groups_data = [
            'id' => '1',
            'user_id' => '1',
            'group_id' => '1'
        ];
        $this->db->insert('users_groups', $groups_data);
        $groups_data = [
            'id' => '2',
            'user_id' => '1',
            'group_id' => '2'
        ];
        $this->db->insert('users_groups', $groups_data);



    }

}
