<?php
/**
 * Migration: Create_ci_sessions
 *
 * Created by: Cli for CodeIgniter <https://github.com/kenjis/codeigniter-cli>
 * Created on: 2016/07/28 10:18:24
 */
class Migration_Create_ci_sessions extends CI_Migration {

	public function up()
	{
//		// Creating a table
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'VARCHAR',
				'constraint' => 40
			),
			'ip_address' => array(
				'type' => 'VARCHAR',
				'constraint' => 45
			),
			'timestamp' => array(
				'type' => 'INT',
				'constraint' => 10,
				'default' => 0,
				'null' => FALSE
			),
			'data' => array(
				'type' => 'BLOB',
				'null' => FALSE
			)
//			'blog_title' => array(
//				'type' => 'VARCHAR',
//				'constraint' => 100,
//			),
//			'blog_author' => array(
//				'type' =>'VARCHAR',
//				'constraint' => '100',
//				'default' => 'King of Town',
//			),
//			'blog_description' => array(
//				'type' => 'TEXT',
//				'null' => TRUE,
//			),
		));
		//$this->dbforge->add_key('timestamp', TRUE);
		$this->dbforge->create_table('ci_sessions');

//		// Adding a Column to a Table
//		$fields = array(
//			'preferences' => array('type' => 'TEXT')
//		);
//		$this->dbforge->add_column('table_name', $fields);
	}

	public function down()
	{
//		// Dropping a table
		$this->dbforge->drop_table('ci_sessions');

//		// Dropping a Column From a Table
//		$this->dbforge->drop_column('table_name', 'column_to_drop');
	}

}
