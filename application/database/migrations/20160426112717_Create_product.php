<?php
/**
 * Migration: Create_product
 *
 * Created by: Cli for CodeIgniter <https://github.com/kenjis/codeigniter-cli>
 * Created on: 2016/04/26 11:27:17
 */
class Migration_Create_product extends CI_Migration {

	public function up()
	{
//		// Creating a table
//		$this->dbforge->add_field(array(
//			'blog_id' => array(
//				'type' => 'INT',
//				'constraint' => 11,
//				'auto_increment' => TRUE
//			),
//			'blog_title' => array(
//				'type' => 'VARCHAR',
//				'constraint' => 100,
//			),
//			'blog_author' => array(
//				'type' =>'VARCHAR',
//				'constraint' => '100',
//				'default' => 'King of Town',
//			),
//			'blog_description' => array(
//				'type' => 'TEXT',
//				'null' => TRUE,
//			),
//		));
//		$this->dbforge->add_key('blog_id', TRUE);
//		$this->dbforge->create_table('blog');

//		// Adding a Column to a Table
//		$fields = array(
//			'preferences' => array('type' => 'TEXT')
//		);
//		$this->dbforge->add_column('table_name', $fields);

		$this->dbforge->add_field(array(
			//ID(プライマリー)
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE
			),
			//商品名
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '64',
			),
			//価格
			'price' => array(
				'type' => 'INT',
				'constraint' => '16',
			),
			//作成者
			'author' => array(
				'type' => 'VARCHAR',
				'constraint' => '64',
			),
			//在庫
			'stock' => array(
				'type' => 'INT',
				'constraint' => '16',
			),
			//商品概要
			'body' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			//商品画像
			'image' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			//商品説明
			'description' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			//作成日
			'created_at' => array(
				'type' => 'timestamp',
				'null' => TRUE
				//'default' => NULL,
			),
			//更新日
			'modified_at' => array(
				'type' => 'datetime',
				'null' => TRUE,
			),
			//削除日
			'deleted_at' => array(
				'type' => 'datetime',
				'null' => TRUE,
			)
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('product');
	}

	public function down()
	{
//		// Dropping a table
//		$this->dbforge->drop_table('blog');

//		// Dropping a Column From a Table
//		$this->dbforge->drop_column('table_name', 'column_to_drop');
	}

}
