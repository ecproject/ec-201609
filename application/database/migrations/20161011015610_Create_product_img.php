<?php
/**
 * Migration: Create_product_img
 *
 * Created by: Cli for CodeIgniter <https://github.com/kenjis/codeigniter-cli>
 * Created on: 2016/10/11 01:56:10
 */
class Migration_Create_product_img extends CI_Migration {

	public function up()
	{
//		// Creating a table
//		$this->dbforge->add_field(array(
//			'blog_id' => array(
//				'type' => 'INT',
//				'constraint' => 11,
//				'auto_increment' => TRUE
//			),
//			'blog_title' => array(
//				'type' => 'VARCHAR',
//				'constraint' => 100,
//			),
//			'blog_author' => array(
//				'type' =>'VARCHAR',
//				'constraint' => '100',
//				'default' => 'King of Town',
//			),
//			'blog_description' => array(
//				'type' => 'TEXT',
//				'null' => TRUE,
//			),
//		));
//		$this->dbforge->add_key('blog_id', TRUE);
//		$this->dbforge->create_table('blog');

//		// Adding a Column to a Table
//		$fields = array(
//			'preferences' => array('type' => 'TEXT')
//		);
//		$this->dbforge->add_column('table_name', $fields);

		$this->dbforge->add_field(array(
			//ID(プライマリー)
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE
			),
			//product-ID
			'product_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			//ファイル名
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			//作成日
			'created_at' => array(
				'type' => 'timestamp',
				'null' => TRUE
				//'default' => NULL,
			)
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('product_img');


	}

	public function down()
	{
//		// Dropping a table
//		$this->dbforge->drop_table('blog');

//		// Dropping a Column From a Table
//		$this->dbforge->drop_column('table_name', 'column_to_drop');
	}

}
