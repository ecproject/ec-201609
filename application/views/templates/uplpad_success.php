<html>
<head>
<title>アップロードフォーム</title>
</head>
<body>

<h3>ファイルのアップロードに成功しました!</h3>

<ul>
{{foreach ($upload_data as $item => $value)}}
<li>{{$item;}}{{$value;}}</li>
{{endforeach;}}
</ul>

<p>{{anchor('upload', 'Upload Another File!');}}</p>

</body>
</html>
