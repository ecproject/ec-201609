<?php

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        //ログインユーザ情報をテンプレートで使用できるようにassign
        if(isset($this->session->userdata)){
            foreach($this->session->userdata as $key=>$value){
                $this->smarty->assign('__'.$key,$value);
            }
        }
        //実行中のクラス名とメソッド
        $this->smarty->assign('__class',$this->router->fetch_class());
        $this->smarty->assign('__method',$this->router->fetch_method());
    }
}
