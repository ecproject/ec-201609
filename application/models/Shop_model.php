<?php
class Shop_model extends CI_Model {

    //コンストラクタ
    public function __construct()
    {
        $this->load->database();
        $this->load->model('product_model');
    }

    /*
     * get_product
     * $name指定：1件取得
     * $name指定なし：全件取得
     */
    public function get_product($name = FALSE){
        if($name == FALSE){
            $query = $this->db->get('product');
            return $query->result_array();
        }
        $query = $this->db->get_where('product',array('name'=>$name));
        return $query->row_array();
    }


    /*検索機能*/

    public function search($keyword = FALSE){
          if($keyword == FALSE){
              $query = $this->db->get('product');
              return $query->result_array();
          }
          $this->db->like('name', $keyword);
          $query = $this->db->get('product');
          return $query->result_array();/*result_arrayで１つだけ取得してくる*/

    // 生成される SQL 文: WHERE title LIKE '%match%'
    }

}
