<?php
class Item_model extends CI_Model {

    //コンストラクタ
    public function __construct()
    {
        $this->load->database();
    }

    /*
     * get_product
     * $name指定：1件取得
     * $name指定なし：全件取得
     */
    public function get_item($name = FALSE){
        if($name == FALSE){
            $query = $this->db->get('item');
            return $query->result_array();
        }
        $query = $this->db->get_where('item',array('name'=>$name));
        return $query->row_array();
    }
}
