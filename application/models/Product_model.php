<?php
class Product_model extends CI_Model {

    //コンストラクタ
    public function __construct()
    {
        $this->load->database();
    }

    /*
     * get_product
     * $name指定：1件取得
     * $name指定なし：全件取得
     */
    public function get_product(){
        $query = $this->db->get_where('product',array('deleted_at'=>NULL));
        return $query->result_array();
    }
    /*
     * get_product
     * $name指定：1件取得
     * $name指定なし：全件取得
     */
    public function get_product_by_id($id = FALSE){
        if($id == FALSE){
            $query = $this->db->get('product');
            return $query->result_array();
        }
        $query = $this->db->get_where('product',array('id'=>$id,'deleted_at'=>NULL));
        return $query->row_array();
    }

    /*
     * set_product
     * $name指定：1件取得
     * $name指定なし：全件取得
     */
    public function set_product($data=array()){

        if(isset($data) && is_array($data)){
            return $this->db->insert('product', $data);
        }
        else{
            return FALSE;
        }

    }


    public function get_category_list()
    {
        $this->db->order_by('id');
        $query = $this->db->get('category');
        return $query->result();
    }

    public function get_category_name($id)
    {
        $this->db->select('name');
        $this->db->where('id', $id);
        $query = $this->db->get('category');
        $row = $query->row();
        return $row->name;
    }

    public function get_product_list($cat_id, $limit, $offset)
    {
        $this->db->where('category_id', $cat_id);
        $this->db->order_by('id');
        $query = $this->db->get('product', $limit, $offset);
        return $query->result();
    }

    public function get_product_count($cat_id)
    {
        $this->db->where('category_id', $cat_id);
        $query = $this->db->get('product');
        return $query->num_rows();
    }

    public function get_product_item($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('product');
        return $query->row();
    }

    public function is_available_product($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('product');
        if ($query->num_rows() == 1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function get_product_by_search($q, $limit, $offset)
    {
        # 検索キーワードをスペースで分割し、like()メソッドでLIKE句を指定します。
        # 複数回like()メソッドを呼んだ場合は、AND条件になります。
        # name LIKE '%{$keyword}%' AND name LIKE '%{$keyword}%' というSQL文になります。
        $keywords = explode(" ", $q);
        foreach ($keywords as $keyword)
        {
            $this->db->like('name', $keyword);
        }
        $this->db->order_by('id');
        $query = $this->db->get('product', $limit, $offset);
        return $query->result();
    }

    public function get_count_by_search($q)
    {
        $this->db->select('name');
        $keywords = explode(" ", $q);
        foreach ($keywords as $keyword)
        {
            $this->db->like('name', $keyword);
        }
        $this->db->order_by('id');
        $query = $this->db->get('product');
        return $query->num_rows();
    }
    /*
     * get_user
     * $name指定：1件取得
     * $name指定なし：全件取得
     */
    public function get_user($name = FALSE){
        if($name == FALSE){
            $query = $this->db->get('user');
            return $query->result_array();
        }
        $query = $this->db->get_where('user',array('name'=>$name));
        return $query->row_array();
    }

//更新・削除処理

    function delete_entry($id)
    {
      $this->db->where('id', $id);
      $this->db->delete('product');
    }

    /*
     * update_users
     * $id指定：1件取得
     * $id指定なし：全件取得
     */
    public function update_product($id = FALSE, $array=array()){
        if($id == FALSE){
            return FALSE;
        }
        if(is_array($array)){
            $this->db->where('id', $id);
            $this->db->update('product', $array);
            return TRUE;
        }

    }


}
