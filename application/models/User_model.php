<?php
class User_model extends CI_Model {

    //コンストラクタ
    public function __construct()
    {
        $this->load->database();
        $this->load->model('product_model');
    }

    /*
     * get_user_by_name
     * $name指定：1件取得
     * $name指定なし：全件取得
     */
    public function get_user_by_name($name = FALSE){
        if($name == FALSE){
            $query = $this->db->get('product');
            return $query->result_array();
        }
        $query = $this->db->get_where('product',array('name'=>$name));
        return $query->row_array();
    }
    /*
     * get_user_by_id
     * $id指定：1件取得
     * $id指定なし：全件取得
     */
    public function get_user_by_id($id = FALSE){
        if($id == FALSE){
            $query = $this->db->get('users');
            return $query->result_array();
        }
        $query = $this->db->get_where('users',array('id'=>$id));
        return $query->row_array();
    }

    /*
     * update_users
     * $id指定：1件取得
     * $id指定なし：全件取得
     */
    public function update_users($id = FALSE, $array=array()){
        if($id == FALSE){
            return FALSE;
        }
        if(is_array($array)){
            $this->db->where('id', $id);
            $this->db->update('users', $array);
            return TRUE;
        }

    }

}
