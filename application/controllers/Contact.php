<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('shop_model');//shopモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * お問い合わせ
     */
    public function index($id = NULL){

        //$this->smarty->assign('product',$data);
        $this->smarty->view('contact/index.html');
    }

}
