<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('shop_model');//shopモデル読み込み
        $this->load->model('cart_model');//cartモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * 一覧（GET）
     * 買い物カゴページ
     */
    public function index($name = NULL){
        //$data = $this->shop_model->get_item($name);

        $data = [];
        //$data['cat_list'] = $this->cart_model->get_category_list();

        # モデルより、カートの情報を取得します。
        $cart = $this->cart_model->get_all();
        $data['total']      = $cart['total'];
        $data['cart']       = $cart['items'];
        $data['item_count'] = $cart['line'];

        $data['main'] = 'shop_cart';

        $this->smarty->assign('item',$data);

        print_r($cart);
        $this->smarty->view('cart/index.html');
    }

    /*
     * カートへ追加
     */
     public function add($item_id=''){
         # 商品IDを検証します。
         /*
 		$this->load->library('validation/field_validation');
 		$this->field_validation->validate(
 			$prod_id, 'required|is_natural|max_length[11]'
 		);
 # POSTされたqtyフィールドより、数量を取得します。
 		$qty = $this->input->post('qty');
 # 数量を検証します。
 		$this->field_validation->validate(
 			$qty, 'required|is_natural|max_length[3]'
 		);
        */
        $qty = $this->input->post('qty');
 		$this->cart_model->add($item_id, $qty);
 # コントローラのindex()メソッドを呼び出し、カートを表示します。
 		$this->index();
     }
}
