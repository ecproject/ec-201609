<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('shop_model');//shopモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * 一覧（GET）
     */
    public function index($id = NULL){
        $data = $this->shop_model->get_product($id);
        //もしデータが無ければ404ページ表示
        if (empty($data))
        {
            //show_404();
        }

        $this->smarty->assign('product',$data);


        if($id){
            $this->smarty->view('shop/detail.html');
        }
        else{
            $this->smarty->view('shop/index.html');
        }
    }
    /*検索機能*/
    public function search($id = NULL){
        $data = $this->shop_model->search($this->input->get('keyword'));
        //print_r($data);
        //もしデータが無ければ404ページ表示
        if (empty($data))
        {
            //show_404();
        }

        $this->smarty->assign('product',$data);


        if($id){
            $this->smarty->view('shop/detail.html');
        }
        else{
            $this->smarty->view('shop/index.html');
        }
    }

    /*cookieお気に入り機能*/
    public function bookmark($id = NULL){
        $data = $this->shop_model->bookmark($this->load->helper('cookie'));
        //print_r($data);
        //もしデータが無ければ404ページ表示
        if (empty($data))
        {
            //show_404();
        }

        $this->smarty->assign('product',$data);


        if($id){
            $this->smarty->view('shop/detail.html');
        }
        else{
            $this->smarty->view('shop/index.html');
        }
    }


}
