<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * HOME
     * ログインTOP
     */
    public function index($name = NULL){
        //$data = $this->shop_model->get_item($name);
        $this->smarty->view('admin/home/index.html');
    }

}
