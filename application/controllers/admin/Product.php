<?php
//商品管理
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
        $this->load->model('product_model');//URLヘルパー読み込み
    }

    /*
     * Product
     * ログインTOP
     */
    public function index($name = NULL){
        $data = $this->product_model->get_product();
        $this->smarty->assign('data',$data);
        //print_r($data);
        $this->smarty->view('admin/product/index.html');
    }

    // create a new product
	public function create()
    {
        $this->data['title'] = $this->lang->line('create_user_heading');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('/admin/product', 'refresh');
        }

        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('name', '商品名', 'required');
        $this->form_validation->set_rules('price', '価格', 'required');
        $this->form_validation->set_rules('author', '作者', 'required|trim');
        $this->form_validation->set_rules('stock', '在庫', 'required|trim');
        $this->form_validation->set_rules('body', '本文', 'required|trim');
        $this->form_validation->set_rules('description', '説明', 'required|trim');

        if ($this->form_validation->run() == true)
        {
            $name         = strtolower($this->input->post('name'));
            $price        = strtolower($this->input->post('price'));
            $author       = strtolower($this->input->post('author'));
            $stock        = strtolower($this->input->post('stock'));
            $body         = strtolower($this->input->post('body'));
            $description  = strtolower($this->input->post('description'));

            $additional_data = array(
                'name'        => $name,
                'price'       => $price,
                'author'      => $author,
                'stock'       => $stock,
                'body'        => $body,
                'description' => $description,
            );
        }
        if ($this->form_validation->run() == true)
        {
            //INSERT Data
            $this->product_model->set_product($additional_data);

            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("/admin/product", 'refresh');
        }
        else
        {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
						//print_r($this->data['message']);
            $this->data['name'] = array(
                'name'  => 'name',
                'id'    => 'name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('name'),
            );
            $this->data['price'] = array(
                'name'  => 'price',
                'id'    => 'price',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('price'),
            );
            $this->data['author'] = array(
                'name'  => 'author',
                'id'    => 'author',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('author'),
            );
            $this->data['stock'] = array(
                'name'  => 'stock',
                'id'    => 'stock',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('stock'),
            );
            $this->data['body'] = array(
                'name'  => 'body',
                'id'    => 'body',
                'type'  => 'textarea',
                'value' => $this->form_validation->set_value('body'),
            );
            $this->data['description'] = array(
                'name'  => 'description',
                'id'    => 'description',
                'type'  => 'textarea',
                'value' => $this->form_validation->set_value('description'),
            );

			//print_r($this->data);
			//
			foreach ($this->data as $key => $value) {
				$this->smarty->assign($key,$value);
			}
			$this->smarty->view('admin/product/create.html');
        }
    }
    // edit a user
	public function edit($id)
	{
		$this->data['title'] = $this->lang->line('edit_user_heading');
		$this->data['id']    = $id;

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect("/admin/product", 'refresh');
		}

		$product = $this->product_model->get_product_by_id($id);

		// validate form input
        $this->form_validation->set_rules('name', '商品名', 'required');
        $this->form_validation->set_rules('price', '価格', 'required');
        $this->form_validation->set_rules('author', '作者', 'required|trim');
        $this->form_validation->set_rules('stock', '在庫', 'required|trim');
        $this->form_validation->set_rules('body', '本文', 'required|trim');
        $this->form_validation->set_rules('description', '説明', 'required|trim');

		if (isset($_POST) && !empty($_POST))
		{

            $name         = strtolower($this->input->post('name'));
            $price        = strtolower($this->input->post('price'));
            $author       = strtolower($this->input->post('author'));
            $stock        = strtolower($this->input->post('stock'));
            $body         = strtolower($this->input->post('body'));
            $description  = strtolower($this->input->post('description'));

			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() === TRUE)
			{
                $data = array(
                    'name'        => $name,
                    'price'       => $price,
                    'author'      => $author,
                    'stock'       => $stock,
                    'body'        => $body,
                    'description' => $description,
                );

			// check to see if we are updating the user
			   if($this->product_model->update_product($id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect("/admin/product", 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect("/admin/product", 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		// display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();
        $this->data['product'] = $product;

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view

        $this->data['name'] = array(
            'name'  => 'name',
            'id'    => 'name',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('name', $product['name']),
        );
        $this->data['price'] = array(
            'name'  => 'price',
            'id'    => 'price',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('price', $product['price']),
        );
        $this->data['author'] = array(
            'name'  => 'author',
            'id'    => 'author',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('author', $product['author']),
        );
        $this->data['stock'] = array(
            'name'  => 'stock',
            'id'    => 'stock',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('stock', $product['stock']),
        );
        $this->data['body'] = array(
            'name'  => 'body',
            'id'    => 'body',
            'type'  => 'textarea',
            'value' => $this->form_validation->set_value('body', $product['body']),
        );
        $this->data['description'] = array(
            'name'  => 'description',
            'id'    => 'description',
            'type'  => 'textarea',
            'value' => $this->form_validation->set_value('description', $product['description']),
        );


		foreach ($this->data as $key => $value) {
			$this->smarty->assign($key,$value);
		}
		$this->smarty->view('admin/product/edit.html');

	}
    public function delete($id)
	{
        //GETされた場合
        $confirm = $this->input->get('confirm');
        if ($_SERVER['REQUEST_METHOD'] == 'GET' && $id && $confirm == TRUE){
            //DELETEの日付をUPDATEする
            $array =  array('deleted_at'=>date('Y-m-d H:i:s'));
            $this->db->where('id', $id);
            $this->db->update('product', $array);

    		redirect("/admin/product", 'refresh');
        }
        else{
            redirect("/admin/product", 'refresh');
        }
	}



    public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

    public function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

}
