

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');//productモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * 一覧（GET）
     */
    public function index($id = NULL){
        $data = $this->shop_model->get_product($id);

        //もしデータが無ければ404ページ表示
        if (empty($data))
        {
            //show_404();
        }

        $this->smarty->assign('product',$data);


        if($id){
            $this->smarty->view('shop/detail.html');
        }
        else{
            $this->smarty->view('shop/index.html');
        }
    }

/*
    /*
     * 新規作成(POST)

    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a new product';

        $this->form_validation->set_rules('name', '商品名', 'required');
        $this->form_validation->set_rules('price', '価格', 'required');//半角数字のみ
        $this->form_validation->set_rules('num', '在庫', 'required');//セレクトボックスで1～100

        //入力失敗
        if ($this->form_validation->run() === FALSE)
        {
            //$this->load->view('templates/header', $data);
            $this->smarty->assign('title',$data['title']);
            $this->smarty->assign('product_num',$this->set_product_num());
            $this->smarty->view('product_new.html');
            //$this->load->view('templates/footer');
        }
        //入力成功
        else
        {
            $this->product_model->set_product();
            $this->smarty->view('product_complete.html');
        }
    }

    public function latest()
	{
		$this->load->view('product_latest');
	}


    private function set_product_num(){
        for($i=1;$i<100;$i++){
            $product_num[] = $i;
        }
        return $product_num;
    }


*/
}
