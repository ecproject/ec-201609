<?php
/*
 * 静的HTML専用
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Static_html extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('shop_model');//shopモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * faq
     */
    public function faq(){
        //$this->smarty->assign('product',$data);
        $this->smarty->view('static_html/faq.html');
    }

    /*
     * 利用規約
     */
    public function terms(){
        //$this->smarty->assign('product',$data);
        $this->smarty->view('static_html/terms.html');
    }

    /*
     * 配送について
     */
    public function shipping(){
        //$this->smarty->assign('product',$data);
        $this->smarty->view('static_html/shipping.html');
    }

}
