<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$smarty = new Smarty;
class Item extends CI_Controller {

  public function __construct(){
        parent::__construct();
        $this->load->model('item_model');//shopモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * 一覧（GET）
     */
  public function index(){
    $data['item'] = $this->item_model->get_item();
    //$this->load->view('item_index',$data);
      $this->smarty->view('item_index.html',$data);
  }

  public function latest()
    {
        $this->load->view('item_latest');
    }


/*
*新規作成
       */
public function new()
{
  $this->load->helper('form');
  $data['title'] = '商品を追加する';

  $this->smarty->assign('title',$data['title']);
  $this->smarty->assign('item_num',$this->set_item_num());
  $this->smarty->view('item_new.html');
}

private function set_item_num(){
  for($i=1;$i<100;$i++){
    $item_num[] = $i;
  }
  return $item_num;
}

}
