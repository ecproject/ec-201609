<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('shop_model');//shopモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * 一覧（GET）
     */
    public function index($id = NULL){
        //$data = $this->shop_model->get_product($id);
        //もしデータが無ければ404ページ表示
        if (empty($data))
        {
            //show_404();
        }

        //$this->smarty->assign('product',$data);
        $this->smarty->view('login/index.html');
    }

}
