<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('product_model');//productモデル読み込み
        $this->load->helper('url_helper');//URLヘルパー読み込み
    }

    /*
     * 一覧（GET）
     */
    public function index($name = NULL){
        $data = $this->product_model->get_product($name);

        //もしデータが無ければ404ページ表示
        if (empty($data))
        {
            //show_404();
        }

        $this->smarty->assign('product',$data);
        $this->smarty->view('product_index.html');
    }

    /*
     * 新規作成(GET)
     */
    public function new()
    {
        $this->load->helper('form');
        $data['title'] = 'Create a new item';

        $this->smarty->assign('title',$data['title']);
        $this->smarty->assign('product_num',$this->set_product_num());
        $this->smarty->view('product_new.html');
    }


    /*
     * 新規作成(POST)
     */
    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a new product';

        $this->form_validation->set_rules('name', '商品名', 'required');
        $this->form_validation->set_rules('price', '価格', 'required');//半角数字のみ
        $this->form_validation->set_rules('num', '在庫', 'required');//セレクトボックスで1～100

        //入力失敗
        if ($this->form_validation->run() === FALSE)
        {
            //$this->load->view('templates/header', $data);
            $this->smarty->assign('title',$data['title']);
            $this->smarty->assign('product_num',$this->set_product_num());
            $this->smarty->view('product_new.html');
            //$this->load->view('templates/footer');
        }
        //入力成功
        else
        {
            $this->product_model->set_product();
            $this->smarty->view('product_complete.html');

            //アップロード処理
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '100';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
              $error = array('error' => $this->upload->display_errors());

              //$this->load->view('upload_form', $error);
            }
            else
            {
              $data = array('upload_data' => $this->upload->data());
            }


            //画像の名前変更処理
            $re=$this->db->insert_id();
            if(isset ($data['upload_data']['full_path']))
            {
              rename($data['upload_data']['full_path'],$data['upload_data']['file_path'].$re.$data['upload_data']['file_ext']);
            }

        }
    }

    public function latest()
	{
		$this->load->view('product_latest');
	}


    private function set_product_num(){
        for($i=1;$i<100;$i++){
            $product_num[] = $i;
        }
        return $product_num;
    }

    //更新処理
    public function update($id=null)
    {
      $this->load->helper('form');
      $this->load->library('form_validation');
      $this->smarty->assign('id',$id);
      $data['title'] = 'Update product';

      $this->form_validation->set_rules('name', '商品名', 'required');
      $this->form_validation->set_rules('price', '価格', 'required');//半角数字のみ
      $this->form_validation->set_rules('num', '在庫', 'required');//セレクトボックスで1～100

      //入力失敗
      if ($this->form_validation->run() === FALSE)
      {
          //$this->load->view('templates/header', $data);
          $this->smarty->assign('title',$data['title']);
          $this->smarty->assign('product_num',$this->set_product_num());
          $this->smarty->view('product_update.html');
          //$this->load->view('templates/footer');
      }
      //入力成功
      else
      {
        //ページにポスト渡し
          $id = $this->input->post("id");
          $this->product_model->update_product($id);
          $this->smarty->view('product_complete.html');

          //アップロード処理
          $config['upload_path'] = './uploads/';
          $config['allowed_types'] = 'gif|jpg|png';
          $config['max_size']	= '100';
          $config['max_width']  = '1024';
          $config['max_height']  = '768';

          $this->load->library('upload', $config);

          if ( ! $this->upload->do_upload())
          {
            $error = array('error' => $this->upload->display_errors());

            //$this->load->view('upload_form', $error);
          }
          else
          {
            $data = array('upload_data' => $this->upload->data());

            //$this->load->view('upload_success', $data);
          }

          //画像の名前変更処理
          $re=$this->db->insert_id();
          rename($data['upload_data']['full_path'],$data['upload_data']['file_path'].$id.$data['upload_data']['file_ext']);

      }

    }


    //削除処理
    public function delete($id)
    {
        $this->product_model->delete_entry($id);
        // PHPと同じフォルダにある「text.txt」が削除されます
        unlink("/uplods");
        header('Location: /product');
        //  $this->smarty->view('product_index.html');
    }

}
